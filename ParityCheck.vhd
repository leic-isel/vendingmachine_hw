----------------------------------------------------------------------------------
-- Company:  		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    20:20:33 11/30/2021 
-- Design Name: 
-- Module Name:    ParityCheck - Structural 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ParityCheck is
    Port ( SCLK : in  STD_LOGIC;
           data : in  STD_LOGIC;
           init : in  STD_LOGIC;
           err  : out STD_LOGIC);
end ParityCheck;

architecture Behavoral of ParityCheck is

	component Register_D is
		Generic (WIDTH : POSITIVE :=1);
		 Port ( CLK : in  STD_LOGIC;
				  RST : in  STD_LOGIC;
				  D   : in  STD_LOGIC_VECTOR (WIDTH -1 downto 0);
				  Q   : out STD_LOGIC_VECTOR (WIDTH -1 downto 0));
	end component;

	signal xor_value : STD_LOGIC;
	signal Q_to_xor  : STD_LOGIC;

begin
	U0_Register_D: Register_D
		Generic map (WIDTH => 1)
		Port map (CLK => SCLK, RST => init, D(0) => xor_value, Q(0) => Q_to_xor);
		
	err       <= not Q_to_xor;
	xor_value <= (data xor Q_to_xor);

end Behavoral;
