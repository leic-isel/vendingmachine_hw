----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    01:10:39 01/21/2022 
-- Design Name: 
-- Module Name:    VendingMachine - Behavioral 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VendingMachine is

	Port (   CLK     : in STD_LOGIC;
				RST     : in STD_LOGIC;	
				
				-- keyboard --------------------------------
				Lines   : in  STD_LOGIC_VECTOR (3 downto 0);
				ACK     : in  STD_LOGIC;
				Cols    : out STD_LOGIC_VECTOR (2 downto 0);
				Qdata   : out STD_LOGIC_VECTOR (3 downto 0);
				Dval    : out STD_LOGIC;
				
				-- integrated output system ------------------
				SCLK    : in  STD_LOGIC;
				SDX     : in  STD_LOGIC;
				Fsh     : in  STD_LOGIC;
				busy    : out STD_LOGIC;
				WrD     : out STD_LOGIC;
				Dout    : out STD_LOGIC_VECTOR (4 downto 0);
				WrL     : out STD_LOGIC;
				
				-- liquid crystal display --------------------------
				RS : in STD_LOGIC;
				D  : in STD_LOGIC_VECTOR ( 3 downto 0 );
				E  : in STD_LOGIC;
				
				-- dispenser ---------------------------------------
				Ej  : in  STD_LOGIC;
				PId : in  STD_LOGIC_VECTOR (3 downto 0);
				Fn  : out STD_LOGIC;
				
				-- maintenance --------------------------------
				Min  : in  STD_LOGIC;
				Mout : out STD_LOGIC);
				
end VendingMachine;

architecture Behavioral of VendingMachine is

	component Keyboardreader is
		 Port ( CLK   : in  STD_LOGIC;
				  RST   : in  STD_LOGIC;
				  Lines : in  STD_LOGIC_VECTOR (3 downto 0);
				  ACK   : in  STD_LOGIC;
				  Cols  : out STD_LOGIC_VECTOR (2 downto 0);
				  Qdata : out STD_LOGIC_VECTOR (3 downto 0);
				  Dval  : out STD_LOGIC);
	end component;
	
	component CoinAcceptor is
		 Port ( coin    : out STD_LOGIC;
				  accept  : in  STD_LOGIC;
				  collect : in  STD_LOGIC;
				  eject   : in  STD_LOGIC);
	end component;
	
	component IOS is
		 Port ( CLK  : in  STD_LOGIC;
				  RST  : in  STD_LOGIC;
				  SCLK : in  STD_LOGIC;
				  SDX  : in  STD_LOGIC;
				  Fsh  : in  STD_LOGIC;
				  busy : out STD_LOGIC;
				  WrD  : out STD_LOGIC;
				  Dout : out STD_LOGIC_VECTOR (4 downto 0);
				  WrL  : out STD_LOGIC);
	end component;
	
	component M is
		 Port ( Min  : in  STD_LOGIC;
				  Mout : out STD_LOGIC);
	end component;
	
begin

	U0_KeyboardReader: Keyboardreader
		Port map (CLK => CLK, RST => RST, Lines => Lines, ACK => ACK,
					 Cols => Cols, Qdata => Qdata, Dval => Dval);
					 
	U1_IOS: IOS
		Port map (CLK => CLK, RST => RST, SCLK => SCLK, SDX => SDX,
					 Fsh => Fsh, busy => busy, WrD => WrD, WrL => WrL,
					 Dout => Dout);
					 
	U2_M: M
		Port map (Min => Min, Mout => Mout);

end Behavioral;
