--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:09:48 01/30/2022
-- Design Name:   
-- Module Name:   C:/Users/lvsit/OneDrive/Ambiente de Trabalho/LIC/vendingmachine_hw - Cpia/SerailCountLogTB.vhd
-- Project Name:  VendingMachine
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: SerialCounterLogic
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY SerailCountLogTB IS
END SerailCountLogTB;
 
ARCHITECTURE behavior OF SerailCountLogTB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT SerialCounterLogic
    PORT(
         operandA : IN  std_logic_vector(2 downto 0);
        -- operandB : IN  std_logic_vector(2 downto 0);
         CLK : IN  std_logic;
         RST : IN  std_logic;
         EN : IN  std_logic;
         R : OUT  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal operandA : std_logic_vector(2 downto 0) := (others => '0');
 --  signal operandB : std_logic_vector(2 downto 0) := (others => '0');
   signal CLK : std_logic := '0';
   signal RST : std_logic := '0';
   signal EN : std_logic := '0';

 	--Outputs
   signal R : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: SerialCounterLogic PORT MAP (
          operandA => operandA,
         -- operandB => operandB,
          CLK => CLK,
          RST => RST,
          EN => EN,
          R => R
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 
		RST <= '0';
		EN <= '1';
		operandA <= "000";
		--operandB <= "001";
		
		operandA <= operandA;
	--	operandB <= '1';
		operandA <= operandA;
	--	operandB <= '1';
		operandA <= operandA;
	--	operandB <= '1';
		operandA <= operandA;
	--	operandB <= '1';
		

      wait;
   end process;

END;
