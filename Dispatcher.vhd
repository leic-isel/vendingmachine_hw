----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:32:24 12/15/2021 
-- Design Name: 
-- Module Name:    Dispatcher - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Dispatcher is
    Port ( CLK : in STD_LOGIC;
			  RST : in STD_LOGIC;
			  Fsh : in  STD_LOGIC;
           Dval : in  STD_LOGIC;
           Din : in  STD_LOGIC_VECTOR (5 downto 0);
           WrD : out  STD_LOGIC;
           WrL : out  STD_LOGIC;
			  --Fn : out STD_LOGIC;
           Dout : out  STD_LOGIC_VECTOR (4 downto 0);
           done : out  STD_LOGIC);
end Dispatcher;


architecture Behavioral of Dispatcher is



component Dispacher_Control is
    Port ( CLK : in STD_LOGIC;
			  RST : in STD_LOGIC;
			  Fsh : in  STD_LOGIC;
           Dval : in  STD_LOGIC;
           Din : in  STD_LOGIC_VECTOR (5 downto 0);
           WrD : out  STD_LOGIC;
           WrL : out  STD_LOGIC;
           Dout : out  STD_LOGIC_VECTOR (4 downto 0);
           done : out  STD_LOGIC;
			  --Fn_out: out STD_LOGIC);
end component;	

--signal Fn_Fsh : STD_LOGIC;
--signal Dout_PId : STD_LOGIC_VECTOR (3 downto 1);

begin

		U0 : Dispacher_Control
			Port map (CLK <= CLK, RST <= RST, Fsh <= Fsh,  Dval <= Dval,
						 Din <= Din, WrD <= WrD, WrL <= WrL, Dout <= Dout, done <= done,);
		
		--U1 : Dispenser
			--Port map (EjOUT <= WrD, Fn <= Fsh, Dout(3 downto 1) <= Dout_PId);

end Behavioral;

