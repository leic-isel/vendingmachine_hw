----------------------------------------------------------------------------------
-- Company: 	    ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    14:43:52 01/22/2022 
-- Design Name: 
-- Module Name:    CoinAcceptor - Structural 
-- Project Name: 	 Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CoinAcceptor is
    Port (coin    : out  STD_LOGIC;
			 coinIn  :  in  STD_LOGIC;			 
          acceptOut : out STD_LOGIC;
			 accept  :  in  STD_LOGIC;
			 collectOut : out STD_LOGIC;
          collect :  in  STD_LOGIC;
			 ejectOut : out STD_LOGIC;
          eject   :  in  STD_LOGIC);
end CoinAcceptor;

architecture Structural of CoinAcceptor is

begin

	coin <= coinIN;
	acceptOut <= accept;
	collectOut <= collect;
	ejectOut <= eject;

end Structural;
