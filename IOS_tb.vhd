--------------------------------------------------------------------------------
-- Company: 		ISEL
-- Engineer:		Grupo 6
--
-- Create Date:   18:47:05 01/25/2022
-- Design Name:   
-- Module Name:   C:/Users/lvsit/OneDrive/Ambiente de Trabalho/vendingmachine_hw/IOS_tb.vhd
-- Project Name:  VendingMachine
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: IOS
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY IOS_tb IS
END IOS_tb;
 
ARCHITECTURE behavior OF IOS_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT IOS
    PORT(
         CLK : IN  std_logic;
         RST : IN  std_logic;
         SCLK : IN  std_logic;
         SDX : IN  std_logic;
         Fsh : IN  std_logic;
         busy : OUT  std_logic;
         WrD : OUT  std_logic;
         Dout : OUT  std_logic_vector(4 downto 0);
         WrL : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal RST : std_logic := '0';
   signal SCLK : std_logic := '0';
   signal SDX : std_logic := '1';  -- #### ALTERADO PARA 1 ###########
   signal Fsh : std_logic := '0';

 	--Outputs
   signal busy : std_logic;
   signal WrD : std_logic;
   signal Dout : std_logic_vector(4 downto 0);
   signal WrL : std_logic;

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
   --constant SCLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: IOS PORT MAP (
          CLK => CLK,
          RST => RST,
          SCLK => SCLK,
          SDX => SDX,
          Fsh => Fsh,
          busy => busy,
          WrD => WrD,
          Dout => Dout,
          WrL => WrL
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process; 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 
		-- vector a 1 0b1111111
		-- start condition
		SCLK <= '0';
		SDX <= '1';
		WAIT FOR clk_period * 3;
		SDX <= '0';
		WAIT FOR clk_period * 3;
		
		
		-- enviar o primeiro bit
		SDX <= '0';
		WAIT FOR clk_period * 3;
		SCLK <= '1';
		wait for CLK_period * 3;
		
		--ENVIO DO 2 BIT
		SDX <= '1';
		wait for CLK_period * 3;
		SCLK <= '0';
		wait for CLK_period * 3;
		SCLK <= '1';
		wait for CLK_period * 3;
		
		--ENVIO DO 3 BIT
		SDX <= '0';
		wait for CLK_period * 3;
		SCLK <= '0';
		wait for CLK_period * 3;
		SCLK <= '1';
		wait for CLK_period * 3;
		
		--ENVIO DO 4 BIT
		SDX <= '1';
		wait for CLK_period * 3;
		SCLK <= '0';
		wait for CLK_period * 3;
		SCLK <= '1';
		wait for CLK_period * 3;
		
		--ENVIO DO 5 BIT
		SDX <= '0';
		wait for CLK_period * 3;
		SCLK <= '0';
		wait for CLK_period * 3;
		SCLK <= '1';
		wait for CLK_period * 3;
		
		--ENVIO DO 6 BIT
		SDX <= '1';
		wait for CLK_period * 3;
		SCLK <= '0';
		wait for CLK_period * 3;
		SCLK <= '1';
		wait for CLK_period * 3;
		
		--ENVIO DO 7 (PARIDADE) BIT
		SDX <= '0';
		wait for CLK_period * 3;
		SCLK <= '0';
		wait for CLK_period * 3;
		SCLK <= '1';
		wait for CLK_period * 3;
		
		
		-- Finalização
		--wait for CLK_period * 10;
		SDX <= '1';
		SCLK <= '0';		
		
      wait;
   end process;

END;
