----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    17:08:46 11/09/2021 
-- Design Name: 
-- Module Name:    KeyBuffer - Structural 
-- Project Name:   Vending Machines
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KeyBuffer is
    Port ( CLK  : in  STD_LOGIC;
           RST  : in  STD_LOGIC;
           D    : in  STD_LOGIC_VECTOR (3 downto 0);
           DAV  : in  STD_LOGIC;
           ACK  : in  STD_LOGIC;
           Q    : out STD_LOGIC_VECTOR (3 downto 0);
           DAC  : out STD_LOGIC;
           Dval : out STD_LOGIC);
end KeyBuffer;

architecture Structural of KeyBuffer is

	component KeyBufferControl is
		 Port ( CLK  : in  STD_LOGIC;
				  RST  : in  STD_LOGIC;
				  DAV  : in  STD_LOGIC;
				  ACK  : in  STD_LOGIC;
				  Wreg : out STD_LOGIC;
				  DAC  : out STD_LOGIC;
				  Dval : out STD_LOGIC);
	end component;

	component Register_D is
		Generic (WIDTH : POSITIVE :=1);
		 Port ( CLK : in  STD_LOGIC;
				  RST : in  STD_LOGIC;
				  D   : in  STD_LOGIC_VECTOR (WIDTH -1 downto 0);
				  Q   : out STD_LOGIC_VECTOR (WIDTH -1 downto 0));
	end component;

	signal writeReg : STD_LOGIC;

begin
	U0_KeyBufferControl: KeyBufferControl
		 Port map (CLK => CLK, RST => RST, DAV => DAV, ACK => ACK, Wreg => writeReg,
					  DAC => DAC, Dval => Dval);
		 
	U1_RegisterD: Register_D
		 Generic map (WIDTH => 4)
		 Port map (CLK => writeReg, RST => RST, D => D, Q => Q);

end Structural;
