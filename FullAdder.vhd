----------------------------------------------------------------------------------
-- Company:        ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    16:26:32 11/02/2021 
-- Design Name: 
-- Module Name:    FullAdder - Behavioral 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FullAdder is
    Port ( A    : in   STD_LOGIC;
           B    : in   STD_LOGIC;
           Cin  : in   STD_LOGIC;
           S    : out  STD_LOGIC;
           Cout : out  STD_LOGIC);
end FullAdder;

architecture Behavioral of FullAdder is

	signal xor_ab : STD_LOGIC;
	signal and_ab : STD_LOGIC;

begin

	xor_ab <= A xor B;
	and_ab <= A and B;
	
	S <= xor_ab xor Cin;
	Cout <= and_ab or (Cin and xor_ab);

end Behavioral;
