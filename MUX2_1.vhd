----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    18:37:31 10/20/2021 
-- Design Name: 
-- Module Name:    MUX2_1 - Behavioral 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MUX2_1 is
	Generic (WIDTH : POSITIVE :=4);
    Port ( I0  : in  STD_LOGIC_VECTOR (width -1 downto 0);
           I1  : in  STD_LOGIC_VECTOR (width -1 downto 0);
           sel : in  STD_LOGIC;
           Y   : out STD_LOGIC_VECTOR (width -1 downto 0));
end MUX2_1;

architecture Behavioral of MUX2_1 is

begin

	Y <= I0 when sel = '0' else I1;

end Behavioral;
