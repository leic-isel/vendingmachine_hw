----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    16:41:10 11/02/2021 
-- Design Name: 
-- Module Name:    Adder4bit - Structural 
-- Project Name: 	 Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adder4bit is
    Port ( A    :  in  STD_LOGIC_VECTOR (3 downto 0);
           B    :  in  STD_LOGIC_VECTOR (3 downto 0);
           Cin  :  in  STD_LOGIC;
           S    : out  STD_LOGIC_VECTOR (3 downto 0);
           Cout : out  STD_LOGIC);
end adder4bit;

architecture Structural of adder4bit is

	component FullAdder
		Port(A    :  in std_logic;
			  B    :  in std_logic;
			  Cin  :  in std_logic;          
			  S    : out std_logic;
			  Cout : out std_logic);
	end component;
	
	signal carry : std_logic_vector(3 downto 1);

begin

	U0_FullAdder1: FullAdder
		 port map (A => A(0), B => B(0), Cin => Cin, S => S(0), Cout => carry(1));
		 
	U1_FullAdder2: FullAdder
		 port map (A => A(1), B => B(1), Cin => carry(1), S => S(1), Cout => carry(2));

	U2_FullAdder3: FullAdder
		 port map (A => A(2), B => B(2), Cin => carry(2), S => S(2), Cout => carry(3));
		 
	U3_FullAdder4: FullAdder
		 port map (A => A(3), B => B(3), Cin => carry(3), S => S(3), Cout => Cout);

end Structural;
