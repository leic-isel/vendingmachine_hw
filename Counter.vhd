----------------------------------------------------------------------------------
-- Company:        ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    14:19:19 11/09/2021 
-- Design Name: 
-- Module Name:    Counter - Structural 
-- Project Name: 	 Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Counter is
    Port ( CLK : in   STD_LOGIC;
           RST : in   STD_LOGIC;
           CE  : in   STD_LOGIC;
           Q   : out  STD_LOGIC_VECTOR (3 downto 0));
end Counter;

architecture Structural of Counter is

	component CounterLogic4bit is
		Port ( operandA : in   STD_LOGIC_VECTOR (3 downto 0);
				  en      : in   STD_LOGIC;
				  R       : out  STD_LOGIC_VECTOR (3 downto 0));
	end component;

	component MUX2_1 is
		 Port ( I0  : in   STD_LOGIC_VECTOR (3 downto 0);
				  I1  : in   STD_LOGIC_VECTOR (3 downto 0);
				  sel : in   STD_LOGIC;
				  Y   : out  STD_LOGIC_VECTOR (3 downto 0));
	end component;

	component Register_D is
		Generic (WIDTH : POSITIVE := 1);
		Port ( CLK : in  STD_LOGIC;
				 RST : in  STD_LOGIC;
				 D   : in  STD_LOGIC_VECTOR (WIDTH -1 downto 0);
				 Q   : out STD_LOGIC_VECTOR (WIDTH -1 downto 0));
	end component;

	signal operandA  : STD_LOGIC_VECTOR (3 downto 0);
	signal result    : STD_LOGIC_VECTOR (3 downto 0);
	signal resultReg : STD_LOGIC_VECTOR (3 downto 0);
	signal compare   : STD_LOGIC;

begin

	U0_CounterLogic4bit: CounterLogic4bit
			Port map (operandA => operandA, en => CE, R => result);
			
	U1_Mux2_1: Mux2_1
			Port map (I0 => result, I1 => "0000", sel => compare, Y => resultReg);
			
	U2_Register_D: Register_D
			Generic map (WIDTH => 4)
			Port map (CLK => CLK, RST => RST, D => resultReg, Q => operandA);
			
	compare <= '1' when operandA = "1100" else '0';
	Q 		  <= operandA;

end Structural;
