----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    17:08:23 11/02/2021 
-- Design Name: 
-- Module Name:    CounterLogic4bit - Behavioral 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CounterLogic4bit is
	Port ( operandA : in  STD_LOGIC_VECTOR (3 downto 0);
           en      : in  STD_LOGIC;
           R       : out STD_LOGIC_VECTOR (3 downto 0));
end CounterLogic4bit;

architecture Structural of CounterLogic4bit is

	component MUX2_1 is
		Port ( I0  : in  STD_LOGIC_VECTOR (3 downto 0);
				 I1  : in  STD_LOGIC_VECTOR (3 downto 0);
				 sel : in  STD_LOGIC;
				 Y   : out STD_LOGIC_VECTOR (3 downto 0));
	end component;

	component Adder4bit is
		Port ( A    : in  STD_LOGIC_VECTOR (3 downto 0);
				 B    : in  STD_LOGIC_VECTOR (3 downto 0);
				 Cin  : in  STD_LOGIC;
				 S    : out STD_LOGIC_VECTOR (3 downto 0);
				 Cout : out STD_LOGIC);
	end component;

	signal operandB : std_logic_vector (3 downto 0);

begin
	U0_MUX2_1: MUX2_1
		Port map (I0 => "0000", I1 => "0001", sel => en, Y => operandB);
	
	U1_Adder4bit: Adder4bit
		Port map (A => operandA, B => operandB, Cin => '0', S => R, Cout => open);

end Structural;
