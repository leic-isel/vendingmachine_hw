----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    12:45:14 11/24/2021 
-- Design Name: 
-- Module Name:    SerialCounterLogic - Structural 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SerialCounterLogic is
	Port ( operandA : in  STD_LOGIC_VECTOR (2 downto 0);
          R        : out STD_LOGIC_VECTOR (2 downto 0));
end SerialCounterLogic;

architecture Structural of SerialCounterLogic is

	component MUX2_1 is
		Generic (WIDTH : POSITIVE :=3);
		Port ( I0  : in STD_LOGIC_VECTOR (2 downto 0);
				 I1  : in STD_LOGIC_VECTOR (2 downto 0);
				 sel : in STD_LOGIC;
				 Y   : out STD_LOGIC_VECTOR (2 downto 0));
	end component;

	component Adder3bit is
		Port ( A    : in STD_LOGIC_VECTOR (2 downto 0);
				 B    : in STD_LOGIC_VECTOR (2 downto 0);
				 Cin  : in STD_LOGIC;
				 Sum  : out STD_LOGIC_VECTOR (2 downto 0);
				 Cout : out STD_LOGIC);
	end component;

	signal operandB : std_logic_vector (2 downto 0);

begin

	U0_MUX : MUX2_1
		Port map (I0 => "000", I1 => "001", sel => en, Y => operandB);
	
	U1_Adder : Adder3bit
		Port map (A => operandA, B => operandB, Cin => '0', Sum => R, Cout => open);

end Structural;
