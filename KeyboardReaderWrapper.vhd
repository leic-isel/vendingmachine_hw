----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    20:41:00 12/21/2021 
-- Design Name: 
-- Module Name:    KeyboardReaderWrapper - Structural 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity KeyboardReaderWrapper is
    Port ( CLK       : in  STD_LOGIC;
           RST       : in  STD_LOGIC;
			  
			  -- KCOLS : OUT  std_logic_vector(2 downto 0);
           J2_9      : out STD_LOGIC;
           J2_10     : out STD_LOGIC;
           J2_11     : out STD_LOGIC;
			  
			  -- KLINS : IN  std_logic_vector(3 downto 0); J2_12 a J2_15
           J2_12     : in  STD_LOGIC;
           J2_13     : in  STD_LOGIC;
           J2_14     : in  STD_LOGIC;
           J2_15     : in  STD_LOGIC;
			  
			  -- ACK 	: IN  std_logic;
           OUTPORT_7 : in  STD_LOGIC;
			  
			  -- D 		: OUT  std_logic_vector(3 downto 0);
           INPORT_0  : out STD_LOGIC;
           INPORT_1  : out STD_LOGIC;
           INPORT_2  : out STD_LOGIC;
           INPORT_3  : out STD_LOGIC;
			  
			  -- DVAL 	: OUT  std_logic
           INPORT_4  : out STD_LOGIC);
end KeyboardReaderWrapper;

architecture Structural of KeyboardReaderWrapper is

	component Keyboardreader is
		 Port ( CLK   : in  STD_LOGIC;
				  RST   : in  STD_LOGIC;
				  Lines : in  STD_LOGIC_VECTOR (3 downto 0);
				  ACK   : in  STD_LOGIC;
				  Cols  : out STD_LOGIC_VECTOR (2 downto 0);
				  Qdata : out STD_LOGIC_VECTOR (3 downto 0);
				  Dval  : out STD_LOGIC);
	end component;

begin
	U0_KeyboardReader: Keyboardreader
		Port map (CLK => CLK, RST => RST, ACK => OUTPORT_7, Dval => INPORT_4,
					 Lines(0) => J2_15, Lines(1) => J2_14, Lines(2) => J2_13, Lines(3) => J2_12,
					 Cols(0) => J2_11, Cols(1) => J2_10, Cols(2) => J2_9,
					 Qdata(0) => INPORT_0, Qdata(1) => INPORT_1,
					 Qdata(2) => INPORT_2, Qdata(3) => INPORT_3);

end Structural;
