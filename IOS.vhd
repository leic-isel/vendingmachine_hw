----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    18:49:05 12/15/2021 
-- Design Name: 
-- Module Name:    IOS - Structural 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IOS is
    Port ( CLK  : in  STD_LOGIC;
			  RST  : in  STD_LOGIC;
			  SCLK : in  STD_LOGIC;
           SDX  : in  STD_LOGIC;
			  Fsh  : in  STD_LOGIC;
			  busy : out STD_LOGIC;
           WrD  : out STD_LOGIC;
           Dout : out STD_LOGIC_VECTOR (4 downto 0);
           WrL  : out STD_LOGIC);
end IOS;

architecture Structural of IOS is
	
	component SerialReceiver is
		 Port ( CLK    : in  STD_LOGIC;
				  RST    : in  STD_LOGIC;
				  SDX    : in  STD_LOGIC;
				  SCLK   : in  STD_LOGIC;
				  accept : in  STD_LOGIC;
				  Data   : out STD_LOGIC_VECTOR (5 downto 0); -- D
				  DXval  : out STD_LOGIC;
				  busy   : out STD_LOGIC);
	end component;
	
	component Dispatcher_Control is
		 Port ( CLK  : in  STD_LOGIC;
				  RST  : in  STD_LOGIC;
				  Fsh  : in  STD_LOGIC;
				  Dval : in  STD_LOGIC;
				  Din  : in  STD_LOGIC_VECTOR (5 downto 0);
				  WrD  : out STD_LOGIC;
				  WrL  : out STD_LOGIC;
				  Dout : out STD_LOGIC_VECTOR (4 downto 0);
				  done : out STD_LOGIC);
	end component;
	
	signal DXval_Dval  : STD_LOGIC;
	signal Data_Din    : STD_LOGIC_VECTOR (5 downto 0);
	signal done_accept : STD_LOGIC;

begin

	U0_SerialReceiver : SerialReceiver
		Port map (CLK => CLK, RST => RST, SDX => SDX, SCLK => SCLK,
					 accept => done_accept, Data => Data_Din,
					 DXval => DXval_Dval, busy => busy);
		
	U1_DispatcherControl : Dispatcher_Control
		Port map (CLK => CLK, RST => RST, Dval => DXval_Dval, Fsh => Fsh, 
					 Din => Data_Din, WrD => WrD, WrL => WrL, Dout => Dout,
					 done => done_accept);

end Structural;
