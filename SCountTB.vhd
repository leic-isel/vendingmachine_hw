--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:46:57 01/30/2022
-- Design Name:   
-- Module Name:   C:/Users/lvsit/OneDrive/Ambiente de Trabalho/LIC/vendingmachine_hw - Cpia/SCountTB.vhd
-- Project Name:  VendingMachine
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: SerialCounter
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY SCountTB IS
END SCountTB;
 
ARCHITECTURE behavior OF SCountTB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT SerialCounter
    PORT(
         CLK : IN  std_logic;
         RST : IN  std_logic;
         operandA : IN  std_logic_vector(2 downto 0);
         en : IN  std_logic;
         R : OUT  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal RST : std_logic := '0';
   signal operandA : std_logic_vector(2 downto 0) := (others => '0');
   signal en : std_logic := '0';

 	--Outputs
   signal R : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: SerialCounter PORT MAP (
          CLK => CLK,
          RST => RST,
          operandA => operandA,
          en => en,
          R => R
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 
		En <= '1';
		OperandA <= "001";
		--wait for CLK_period * 3;
		OperandA <= "001";
		--wait for CLK_period * 3;
		OperandA <= "001";
		--wait for CLK_period * 3;
		RST <= '1';
		OperandA <= "001";

      wait;
   end process;

END;
