----------------------------------------------------------------------------------
-- Company:  		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    19:51:52 12/13/2021 
-- Design Name: 
-- Module Name:    DataStorage - Structural 
-- Project Name: 	 Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DataStorage is
    Port ( CLK    : in   STD_LOGIC;
           data   : in   STD_LOGIC;
           enable : in   STD_LOGIC;
           Ain    : in   STD_LOGIC_VECTOR (2 downto 0);
           Dout   : out  STD_LOGIC_VECTOR (5 downto 0));
end DataStorage;

architecture Structural of DataStorage is

	component Register_D_En is
		Port ( CLK : in  STD_LOGIC;
				 RST : in  STD_LOGIC;
				 En  : in  STD_LOGIC;
				 D   : in  STD_LOGIC;
				 Q   : out STD_LOGIC);
	end component;

	component DEMUX is
		 Port ( S : in  STD_LOGIC_VECTOR (2 downto 0);
				  F : in  STD_LOGIC;
				  O : out STD_LOGIC_VECTOR (5 downto 0));
	end component;


	signal signal_Q : STD_LOGIC_VECTOR (5 downto 0);
	signal signal_O : STD_LOGIC_VECTOR (5 downto 0);

begin

	U0_RegBit0: Register_D_En
		Port map (CLK => CLK, RST => '0', En => signal_O(0), D => data, Q => signal_Q(0));
		
	U1_RegBit1: Register_D_En
		Port map (CLK => CLK, RST => '0', En => signal_O(1), D => data, Q => signal_Q(1));
		
	U2_RegBit2: Register_D_En
		Port map (CLK => CLK, RST => '0', En => signal_O(2), D => data, Q => signal_Q(2));
		
	U3_RegBit3: Register_D_En
		Port map (CLK => CLK, RST => '0', En => signal_O(3), D => data, Q => signal_Q(3));

	U4_RegBit4: Register_D_En
		Port map (CLK => CLK, RST => '0', En => signal_O(4), D => data, Q => signal_Q(4));
		
	U5_RegBit5: Register_D_En
		Port map (CLK => CLK, RST => '0', En => signal_O(5), D => data, Q => signal_Q(5));
		
	U6_DEMUX: DEMUX
		Port map (F => enable, S => Ain, O => signal_O);

	Dout <= signal_Q;

end Structural;
