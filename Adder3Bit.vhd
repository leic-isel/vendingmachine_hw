----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    12:28:22 11/24/2021 
-- Design Name: 
-- Module Name:    Adder3Bit - Structural 
-- Project Name: 	 Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Adder3Bit is
    Port ( A : in  STD_LOGIC_VECTOR (2 downto 0);
           B : in  STD_LOGIC_VECTOR (2 downto 0);
           Cin : in  STD_LOGIC;
           Sum : out  STD_LOGIC_VECTOR (2 downto 0);
           Cout : out  STD_LOGIC);
	end Adder3Bit;

architecture Structural of Adder3Bit is

	component FullAdder is
		Port (A : in  STD_LOGIC;
				B : in  STD_LOGIC;
				Cin : in  STD_LOGIC;
				S : out  STD_LOGIC;
				Cout : out  STD_LOGIC);
	end component;

	signal carry :STD_LOGIC_VECTOR (1 downto 0);

begin
	U0_FullAdder1 : FullAdder 
			Port map (A => A(0), B => B(0),  Cin => Cin, S => Sum(0), Cout => carry(0));

	U1_FullAdder2 : FullAdder 
			Port map (A => A(1), B => B(1), Cin => carry(0), S => Sum(1), Cout => carry(1));

	U2_FullAdder3 : FullAdder 
			Port map(A => A(2), B => B(2), Cin => carry(1), S => Sum(2), Cout => Cout);

end Structural;

