----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    17:21:39 11/09/2021 
-- Design Name: 
-- Module Name:    Keyboardreader - Structural 
-- Project Name: 	 Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Keyboardreader is
    Port ( CLK   : in  STD_LOGIC;
           RST   : in  STD_LOGIC;
           Lines : in  STD_LOGIC_VECTOR (3 downto 0);
           ACK   : in  STD_LOGIC;
           Cols  : out STD_LOGIC_VECTOR (2 downto 0);
           Qdata : out STD_LOGIC_VECTOR (3 downto 0);
           Dval  : out STD_LOGIC);
end Keyboardreader;

architecture Structural of Keyboardreader is

	component  KeyDecode is
		 Port ( CLK   : in  STD_LOGIC;
				  RST   : in  STD_LOGIC;
				  Kack  : in  STD_LOGIC;
				  Lines : in  STD_LOGIC_VECTOR (3 downto 0);
				  Kval  : out STD_LOGIC;
				  D     : out STD_LOGIC_VECTOR (3 downto 0);
				  Cols  : out STD_LOGIC_VECTOR (2 downto 0));
	end component;

	component  KeyBuffer is
		 Port ( CLK  : in  STD_LOGIC;
				  RST  : in  STD_LOGIC;
				  D    : in  STD_LOGIC_VECTOR (3 downto 0);
				  DAV  : in  STD_LOGIC;
				  ACK  : in  STD_LOGIC;
				  Q    : out STD_LOGIC_VECTOR (3 downto 0);
				  DAC  : out STD_LOGIC;
				  Dval : out STD_LOGIC);
	end component;

	signal signal_Kack   : STD_LOGIC;
	signal signal_Kpress : STD_LOGIC;
	signal signal_Kdata  : STD_LOGIC_VECTOR (3 downto 0);

begin 

	U0_KeyDecode: KeyDecode
		 Port map (CLK => CLK, RST => RST, Kack => signal_Kack, Lines => Lines, 
					  Kval => signal_Kpress, D => signal_Kdata, Cols => Cols);

	U1_KeyBuffer: KeyBuffer
		 Port map (CLK => CLK, RST => RST, D => signal_Kdata, DAV => signal_Kpress,
					  ACK => ACK, Q => Qdata, DAC => signal_Kack, Dval => Dval);

end Structural;
