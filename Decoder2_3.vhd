----------------------------------------------------------------------------------
-- Company:			 ISEL 
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    22:16:10 10/26/2021 
-- Design Name: 
-- Module Name:    Decoder2_3 - Behavioral 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Decoder2_3 is
    Port ( S : in  STD_LOGIC_VECTOR (1 downto 0);
           O : out  STD_LOGIC_VECTOR (2 downto 0));
end Decoder2_3;

architecture Behavioral of Decoder2_3 is

begin
	
	process (S)
		begin	
			if    (S = "00") then O <= "001";
			elsif (S = "01") then O <= "010";
			elsif (S = "10") then O <= "100";
			else 						 O <= "111";
			end if;
	end process;

end Behavioral;
