----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:31:35 10/26/2021 
-- Design Name: 
-- Module Name:    Counter_4bit - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity counter_4bit is
    Port ( osc : in  STD_LOGIC;
           ce : in  STD_LOGIC;
           q : out  STD_LOGIC_VECTOR (3 downto 0));
end counter_3bit;

architecture Structural of counter_3bit is

--COMPONENT CounterLogic_3bit
--	PORT(
--		operandA : IN std_logic_vector(2 downto 0);
--		ndecINC : IN std_logic;
--		en : IN std_logic;          
--		R : OUT std_logic_vector(2 downto 0)
--		);
--	END COMPONENT;
--
--COMPONENT register_D_R
--	generic (WIDTH : POSITIVE :=1);
--	PORT( CLK : IN std_logic;
--			RST : IN std_logic;
--			D : IN std_logic_vector(WIDTH-1 downto 0);          
--			Q : OUT std_logic_vector(WIDTH-1 downto 0));
--END COMPONENT;
--
--signal operandA, result : std_logic_vector(2 downto 0);
--
--begin
--
--U0 : CounterLogic_3bit
--	Port map ( operandA => operandA, ndecINC => ndecINC, en => en, R => result );
--	
--U1 : register_D_R
--	Generic  map ( WIDTH => 3 )
--	Port map ( CLK => clk, RST => rst, D => result, Q => operandA );
--
--q <= operandA;

end Structural;

