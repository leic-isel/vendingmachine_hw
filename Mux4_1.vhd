----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    21:00:00 10/26/2021 
-- Design Name: 
-- Module Name:    Mux4_1 - Behavioral 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mux4_1 is
    Port ( S0 : in  STD_LOGIC;
           S1 : in  STD_LOGIC;
           I  : in  STD_LOGIC_VECTOR (3 downto 0);
           Y  : out STD_LOGIC);
end Mux4_1;

architecture Behavioral of Mux4_1 is

begin
	process (S0, S1, I)
		begin

			if    (S0 = '0' and S1 = '0') then Y <= I(0);
			elsif (S0 = '1' and S1 = '0') then Y <= I(1);
			elsif (S0 = '0' and S1 = '1') then Y <= I(2);
			else                               Y <= I(3);
			end if;
			
		end process;

end Behavioral;
