----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    20:55:04 10/26/2021 
-- Design Name: 
-- Module Name:    KeyScan - Behavioral
-- Project Name: 	 Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KeyScan is
    Port ( CLK    : in  STD_LOGIC;
			  RST    : in  STD_LOGIC;
			  Kscan  : in  STD_LOGIC;
			  Rows   : in  STD_LOGIC_VECTOR (3 downto 0);
           Cols   : out STD_LOGIC_VECTOR (2 downto 0);
           Kdata  : out STD_LOGIC_VECTOR (3 downto 0);
           Kpress : out STD_LOGIC);
end KeyScan;

architecture Structural of KeyScan is

	component Counter is
		Port ( CLK : in  STD_LOGIC;
				 RST : in  STD_LOGIC;
				 CE  : in  STD_LOGIC;
				 Q   : out STD_LOGIC_VECTOR (3 downto 0));
	end component;

	component Decoder2_3 is
		Port ( S : in  STD_LOGIC_VECTOR (1 downto 0);
				 O : out STD_LOGIC_VECTOR (2 downto 0));			 
	end component;

	component Mux4_1 is
		Port 	( S0 : in  STD_LOGIC;
				  S1 : in  STD_LOGIC;
				  I  : in  STD_LOGIC_VECTOR (3 downto 0);
				  Y  : out STD_LOGIC);
	end component;
				  
	signal decoder     : STD_LOGIC_VECTOR (1 downto 0);
	signal mux         : STD_LOGIC_VECTOR (1 downto 0);
	signal out_decoder : STD_LOGIC_VECTOR (2 downto 0);
	signal out_kpress  : STD_LOGIC;

begin

	U0_Counter: Counter
		Port map ( CLK => not CLK, RST => RST, CE => Kscan,
					  Q(3) => decoder(1), Q(2) => decoder(0), Q(1) => mux(1), Q(0) => mux(0) );
		
	U1_Decoder2_3: Decoder2_3
		Port map ( S(1) => decoder(1), S(0) => decoder(0), O => out_decoder);
		
	U2_Mux4_1: Mux4_1
		Port map ( I(0) => Rows(0), I(1) => Rows(1), I(2) => Rows(2), I(3) => Rows(3),
					  S1 => mux(1), S0 => mux(0), Y => out_kpress);

	Kdata(3) <= decoder(1);
	Kdata(2) <= decoder(0);
	Kdata(1) <= mux(1);
	Kdata(0) <= mux(0);
	Cols     <= not out_decoder;
	Kpress   <= not out_kpress;

end Structural;
