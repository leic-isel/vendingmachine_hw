----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    22:33:17 11/23/2021 
-- Design Name: 
-- Module Name:    SerialCounter - Structural 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SerialCounter is
    Port ( CLK     : in  STD_LOGIC;
           CLR     : in  STD_LOGIC;
           Qresult : out STD_LOGIC_VECTOR (2 downto 0));
end SerialCounter;

architecture Structural of SerialCounter is

	component Adder3Bit is
			Port (A    : in  STD_LOGIC_VECTOR (2 downto 0);
					B    : in  STD_LOGIC_VECTOR (2 downto 0);
					Cin  : in  STD_LOGIC;
					Cout : out STD_LOGIC;
					Sum  : out STD_LOGIC_VECTOR (2 downto 0));
	end component;

	component MUX2_1 is
		Generic (WIDTH : POSITIVE :=3);
		 Port ( I0  : in  STD_LOGIC_VECTOR (2 downto 0);
				  I1  : in  STD_LOGIC_VECTOR (2 downto 0);
				  sel : in  STD_LOGIC;
				  Y   : out STD_LOGIC_VECTOR (2 downto 0));
	end component;

	component Register_D is
		Generic (WIDTH : POSITIVE := 1);
		Port ( CLK : in  STD_LOGIC;
				 RST : in  STD_LOGIC;
				 D   : in  STD_LOGIC_VECTOR (WIDTH -1 downto 0);
				 Q   : out STD_LOGIC_VECTOR (WIDTH -1 downto 0));
	end component;

	signal resultReg : STD_LOGIC_VECTOR (2 downto 0);
	signal Dsignal   : STD_LOGIC_VECTOR (2 downto 0);

begin

	U0_Adder : Adder3bit
			Port map (A => resultReg, B => "001", Cin => '0', Sum => Dsignal, Cout => open);
			
	U2_Register_D: Register_D
			Generic map (WIDTH => 3)
			Port map (CLK => CLK, RST => CLR, D => Dsignal, Q => resultReg);
			
	Qresult <= resultReg;

end Structural;
