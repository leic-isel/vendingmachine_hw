----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    21:46:48 12/14/2021 
-- Design Name: 
-- Module Name:    DEMUX - Behavioral 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DEMUX is
    Port ( S : in  STD_LOGIC_VECTOR (2 downto 0);
           F : in  STD_LOGIC;
           O : out STD_LOGIC_VECTOR (5 downto 0));
end DEMUX;

architecture Behavioral of DEMUX is

begin

   process(F, S)
	
		begin
			if (F = '1') then
				case (S) is
					when "000"  =>  O <= "000001";
					when "001"  =>  O <= "000010";
					when "010"  =>  O <= "000100";
					when "011"  =>  O <= "001000";
					when "100"  =>  O <= "010000";
					when "101"  =>  O <= "100000";
					when others =>  O <= "000000";
				end case;
			else
				O <= "000000";
			end if;
			
	end process;

end Behavioral;
