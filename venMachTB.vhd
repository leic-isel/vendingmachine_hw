--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:49:39 02/01/2022
-- Design Name:   
-- Module Name:   C:/Users/lvsit/OneDrive/Ambiente de Trabalho/LIC/vendingmachine_hw - Cpia/venMachTB.vhd
-- Project Name:  VendingMachine
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: VendingMachine
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY venMachTB IS
END venMachTB;
 
ARCHITECTURE behavior OF venMachTB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT VendingMachine
    PORT(
         CLK     :  IN  std_logic;
         RST     :  IN  std_logic;
         Lines   :  IN  std_logic_vector(3 downto 0);
         ACK     :  IN  std_logic;
         Cols    : OUT  std_logic_vector(2 downto 0);
         Qdata   : OUT  std_logic_vector(3 downto 0);
         Dval    : OUT  std_logic;
         coin    : OUT  std_logic;
         accept  :  IN  std_logic;
         collect :  IN  std_logic;
         eject   :  IN  std_logic;
         SCLK    :  IN  std_logic;
         SDX     :  IN  std_logic;
         Fsh     :  IN  std_logic;
         busy    : OUT  std_logic;
         WrD     : OUT  std_logic;
         Dout    : OUT  std_logic_vector(4 downto 0);
         WrL     : OUT  std_logic;
         RS      :  IN  std_logic;
         D       :  IN  std_logic_vector(3 downto 0);
         E       :  IN  std_logic;
         Ej      :  IN  std_logic;
         PId     :  IN  std_logic_vector(3 downto 0);
         Fn      : OUT  std_logic;
         Min     :  IN  std_logic;
         Mout    : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal CLK     : std_logic := '0';
   signal RST     : std_logic := '0';
   signal Lines   : std_logic_vector(3 downto 0) := (others => '0');
   signal ACK     : std_logic := '0';
   signal accept  : std_logic := '0';
   signal collect : std_logic := '0';
   signal eject   : std_logic := '0';
   signal SCLK    : std_logic := '0';
   signal SDX     : std_logic := '0';
   signal Fsh     : std_logic := '0';
   signal RS      : std_logic := '0';
   signal D       : std_logic_vector(3 downto 0) := (others => '0');
   signal E       : std_logic := '0';
   signal Ej      : std_logic := '0';
   signal PId     : std_logic_vector(3 downto 0) := (others => '0');
   signal Min     : std_logic := '0';

 	--Outputs
   signal Cols  : std_logic_vector(2 downto 0);
   signal Qdata : std_logic_vector(3 downto 0);
   signal Dval  : std_logic;
   signal coin  : std_logic;
   signal busy  : std_logic;
   signal WrD   : std_logic;
   signal Dout  : std_logic_vector(4 downto 0);
   signal WrL   : std_logic;
   signal Fn    : std_logic;
   signal Mout  : std_logic;

   -- Clock period definitions
   constant CLK_period  : time := 10 ns;
   constant SCLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: VendingMachine PORT MAP (
          CLK     => CLK,
          RST     => RST,
          Lines   => Lines,
          ACK     => ACK,
          Cols    => Cols,
          Qdata   => Qdata,
          Dval    => Dval,
          coin    => coin,
          accept  => accept,
          collect => collect,
          eject   => eject,
          SCLK    => SCLK,
          SDX     => SDX,
          Fsh     => Fsh,
          busy    => busy,
          WrD     => WrD,
          Dout    => Dout,
          WrL     => WrL,
          RS      => RS,
          D       => D,
          E       => E,
          Ej      => Ej,
          PId     => PId,
          Fn      => Fn,
          Min     => Min,
          Mout    => Mout
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		SDX <= '1';

      wait for CLK_period*10;

      -- insert stimulus here		
		wait for CLK_period*3;
		Lines <= "1111";
		SCLK <= '0';
		SDX <= '1';
		wait for CLK_period*3;
		SDX <= '0';
		
		-- bit 1
		wait for CLK_period*3;
		SDX <= '1';
		wait for CLK_period*3;
		SCLK <= '1';
		
		-- BIT 2
		wait for CLK_period*3;
		SCLK <= '1';
		wait for CLK_period*3;
		SDX <= '1';
		SCLK <= '0';
		
		-- BIT 3
		wait for CLK_period*3;
		SCLK <= '1';
		wait for CLK_period*3;
		SDX <= '1';
		SCLK <= '0';
		
		-- BIT 4
		wait for CLK_period*3;
		SCLK <= '1';
		SDX <= '1';
		wait for CLK_period*3;
		SCLK <= '0';
		
		-- BIT 5
		wait for CLK_period*3;
		SCLK <= '1';
		wait for CLK_period*3;
		SDX <= '1';
		SCLK <= '0';
		
		-- BIT 6
		wait for CLK_period*3;
		SCLK <= '1';
		wait for CLK_period*3;
		SDX <= '0';
		SCLK <= '0';
		
		-- BIT 7
		wait for CLK_period*3;
		SCLK <= '1';
		wait for CLK_period*3;
		SDX <= '1';
		SCLK <= '0';
		
		wait for CLK_period*3;
		SCLK <= '1';
		wait for CLK_period*3;
		SDX <= '1';
		SCLK <= '0';

      wait;
   end process;

END;
