----------------------------------------------------------------------------------
-- Company:			 ISEL 
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    05:33:15 12/12/2021 
-- Design Name: 
-- Module Name:    Compare5 - Behavioral 
-- Project Name: 	 Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Compare5 is
    Port (input  :  in  STD_LOGIC_VECTOR (2 downto 0);
          output : out  STD_LOGIC);
end Compare5;

architecture Behavioral of Compare5 is

begin

	output <= '1' when input = "101" else '0';

end Behavioral;
