----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:29:47 12/14/2021 
-- Design Name: 
-- Module Name:    Decoder3_6 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Decoder3_6 is
    Port ( S : in  STD_LOGIC_VECTOR (2 downto 0);
           O : out  STD_LOGIC_VECTOR (5 downto 0));
end Decoder3_6;

architecture Behavioral of Decoder3_6 is

begin
	
	process (S)
		begin
			if (S = "001") then O <= "000001";
			elsif (S = "010") then O <= "000010";
			elsif (S = "100") then O <= "000100";
			elsif (S = "011") then O <= "001000";
			elsif (S = "101") then O <= "010000";
			elsif (S = "110") then O <= "100000";
			else O <= "000000";
			end if;
	end process;


end Behavioral;

