----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer:       Grupo 6
-- 
-- Create Date:    14:08:45 01/23/2022 
-- Design Name: 
-- Module Name:    VendingMachineWraper - Structural 
-- Project Name:   Vending Machine 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VendingMachineWraper is

    Port ( CLK : in STD_LOGIC;
			  RST : in STD_LOGIC;
			  
			  -- keyboard ------------------------------------------
			  J2_9: out STD_LOGIC; --cols 2
			  J2_10: out STD_LOGIC; -- cols 1
			  J2_11: out STD_LOGIC; -- cols 0
			  J2_12: in  STD_LOGIC; --lines 0
			  J2_13: in  STD_LOGIC; --line 1
			  J2_14: in  STD_LOGIC; -- lines 2
			  J2_15: in  STD_LOGIC; -- lines 3
			  --J2_6      : in  STD_LOGIC; -- Lines(3)
           --J2_7      : in  STD_LOGIC; -- Lines(2)
           --J2_8      : in  STD_LOGIC; -- Lines(1)
           --J2_9      : in  STD_LOGIC; -- Lines(0)
           OUTPORT_7 : in  STD_LOGIC; --ACK
           --J2_10     : out STD_LOGIC; -- Cols(2)
           --J2_11     : out STD_LOGIC; -- Cols(1)
           --J2_12     : out STD_LOGIC; -- Cols(0)
			  INPORT_4  : out STD_LOGIC; -- Dval 
           INPORT_3  : out STD_LOGIC; -- Qdata(3)
           INPORT_2  : out STD_LOGIC; -- Qdata(2)
           INPORT_1  : out STD_LOGIC; -- Qdata(1)
           INPORT_0  : out STD_LOGIC; -- Qdata(0)
			  
			  -- integrated output system ---------------------------
           OUTPORT_2 : in  STD_LOGIC; -- SCLK
           OUTPORT_1 : in  STD_LOGIC; -- SDX
			  J1_17     : in  STD_LOGIC; -- Fsh
           INPORT_6  : out STD_LOGIC; -- busy
           J1_11     : out STD_LOGIC; -- WrD;
			  J1_5      : out STD_LOGIC; -- Dout(0) RS
           J1_6      : out STD_LOGIC; -- Dout(1) PIN 11 D4
           J1_7      : out STD_LOGIC; -- Dout(2)PIN 12 D5
           J1_8      : out STD_LOGIC; -- Dout(3) PIN 13 d6
           J1_9      : out STD_LOGIC; -- Dout(4) PIN 14 D7
           J1_10     : out STD_LOGIC; -- WrL  E
		  
			  -- mantenance ------------------------------------------
           J4_13    : in  STD_LOGIC; -- Min
           INPORT_7 : out STD_LOGIC; -- Mout
			  
			  -- coin acceptor ---------------------------------------
           J3_15 : in STD_LOGIC; -- coin
           J3_14 : out  STD_LOGIC; -- accept
           J3_13 : out  STD_LOGIC; -- collect
           J3_12 : out  STD_LOGIC; -- eject
			  INPORT_5 : out STD_LOGIC; -- coinin
           OUTPORT_4 : in  STD_LOGIC; -- acceptOut
           OUTPORT_5 : in  STD_LOGIC; -- collectOut
           OUTPORT_6 : in  STD_LOGIC); -- ejectOut
			  
end VendingMachineWraper;

architecture Structural of VendingMachineWraper is

	component VendingMachine is
		Port (   CLK     : in  STD_LOGIC;
					RST     : in  STD_LOGIC;
					
					-- keyboard ----------------------------------------
					Lines   : in  STD_LOGIC_VECTOR (3 downto 0);
					ACK     : in  STD_LOGIC;
					Cols    : out STD_LOGIC_VECTOR (2 downto 0);
					Qdata   : out STD_LOGIC_VECTOR (3 downto 0);
					Dval    : out STD_LOGIC;
					
					-- integrated output system ------------------------
					SCLK    : in  STD_LOGIC;
					SDX     : in  STD_LOGIC;
					Fsh     : in  STD_LOGIC;
					busy    : out STD_LOGIC;
					WrD     : out STD_LOGIC;
					Dout    : out STD_LOGIC_VECTOR (4 downto 0);
					WrL     : out STD_LOGIC;

					-- maintenance -------------------------------------
					Min  : in  STD_LOGIC;
					Mout : out STD_LOGIC);	
	
	end component;	

begin

	J3_14  <=  OUTPORT_4;
	INPORT_5 <= J3_15; 
	J3_13 <= OUTPORT_5; 
	J3_12 <= OUTPORT_6;

	U0_VendingMachine: VendingMachine
		Port map	( CLK => CLK, RST => RST,
		
			  -- keyboard ---------------------------------------
			  ACK => OUTPORT_7, Dval => INPORT_4,
			  Cols(0) => J2_11, Cols(1) => J2_10, Cols(2) => J2_9,
			  Lines(0) => J2_12, Lines(1) => J2_13, Lines(2) => J2_14, Lines(3) => J2_15,
			  Qdata(0) => INPORT_0, Qdata(1) => INPORT_1,
			  Qdata(2) => INPORT_2, Qdata(3) => INPORT_3,
			  
			  -- integred output system --------------------------
			  SCLK => OUTPORT_2, SDX => OUTPORT_1,
			  busy => INPORT_6, WrD => J1_11, WrL  => J1_10, Fsh => J1_17,
			  Dout(0) => J1_5, Dout(1) => J1_6, Dout(2) => J1_7, Dout(3) => J1_8, 
			  Dout(4) => J1_9,
			  
			  -- maintenance -------------------------------------
			  Min => J4_13, Mout => INPORT_7 );
			  

end Structural;
