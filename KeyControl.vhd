----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    15:51:10 11/09/2021 
-- Design Name: 
-- Module Name:    KeyControl - Behavioral 
-- Project Name: 	 Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KeyControl is
    Port ( CLK    : in  STD_LOGIC;
           RST    : in  STD_LOGIC;
           Kack   : in  STD_LOGIC;
           Kpress : in  STD_LOGIC;
           Kval   : out STD_LOGIC;
           Kscan  : out STD_LOGIC);
end KeyControl;

architecture Behavioral of KeyControl is

	type STATE_TYPE is (STATE_KSCAN, STATE_KVAL, STATE_DONE);

	signal CS, NS: STATE_TYPE;

begin
	State_Transitions : process (CLK, RST)
		begin
			if (RST = '1') then CS <= STATE_KSCAN;
			elsif rising_edge(CLK) then CS <= NS;
			end if;
		end process;

	Next_State_Evaluation : process (CS, Kpress, Kack)
		begin
			case (CS) is
				when STATE_KSCAN => if (Kpress = '1') then NS <= STATE_KVAL;
										  else NS <= STATE_KSCAN;
										  end if;
										  
				when STATE_KVAL  => if (Kack = '1') then NS <= STATE_DONE;
										  else NS <= STATE_KVAL;
										  end if;
										 
				when STATE_DONE  => if (Kack = '1') or (Kpress = '1') then NS <= STATE_DONE;
										  else NS <= STATE_KSCAN;
										  end if;
			end case;
		end process;

   -- signal assigments statements
	Kval  <= '1' when (CS = STATE_KVAL)  else '0';
	Kscan <= '1' when (CS = STATE_KSCAN) else '0';
	
end Behavioral;
