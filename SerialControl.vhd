----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    22:29:47 11/23/2021 
-- Design Name: 
-- Module Name:    SerialControl - Behavioral 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SerialControl is
    Port ( CLK     : in  STD_LOGIC;
			  RST     : in  STD_LOGIC;
			  SDX     : in  STD_LOGIC;
			  SCLK    : in  STD_LOGIC;
			  accept  : in  STD_LOGIC;
			  pFlag   : in  STD_LOGIC;
			  dFlag   : in  STD_LOGIC;
			  RXerror : in  STD_LOGIC;
           wr      : out STD_LOGIC; 
           init    : out STD_LOGIC; 
           DXval   : out STD_LOGIC; 
           busy    : out STD_LOGIC); 
end SerialControl;

architecture Behavioral of SerialControl is	

	type STATE_TYPE   is (STATE_IDLE, STATE_WAIT_INIT, STATE_START);
	type STATE_TYPE_2 is (STATE_IDLE_2, STATE_INIT, STATE_RCV_DATA, STATE_RCV_P,
								 STATE_CHK_DX, STATE_TRANSMIT_DX, STATE_WAIT_END);
	
	signal signalStart : STD_LOGIC;
	signal CS, NS      : STATE_TYPE;
	signal CS2, NS2    : STATE_TYPE_2;

	begin
	
		-- FINITE STATE MACHINE 1 ----------------------------------
		State_Transitions: process (CLK, RST, NS) is
			begin
				if (RST = '1') then CS <= STATE_IDLE;
				elsif rising_edge(CLK) then CS <= NS;
				end if;
			end process;
			
		Next_State_Evaluation: process (SCLK, SDX, CS) is
			begin
				case (CS) is
					when STATE_IDLE 	   => if ((SCLK = '0') and (SDX = '1')) then NS <= STATE_WAIT_INIT;
												   else NS <= STATE_IDLE;
												   end if;
												  
					when STATE_WAIT_INIT => if (SCLK = '1') then NS <= STATE_IDLE;
													elsif (SDX = '1') then NS <= STATE_WAIT_INIT;
													else NS <= STATE_START;
													end if;
													
					when STATE_START     => NS <= STATE_IDLE;
					
				end case;
			end process; 
			
		signalSTART <= '1' when (CS = STATE_START) else '0';
			
		-- FINITE STATE MACHINE 2 ----------------------------------
		State_Transitions_2: process (CLK, RST, signalStart, NS2) is 
			begin
				if (RST = '1' or signalStart = '1') then CS2 <= STATE_INIT;
				elsif rising_edge(CLK) then CS2 <= NS2;
				end if;
			end process;
		
		Next_State_Evaluation_2: process (CS2, SCLK, SDX, dFlag, pFlag, RXerror, accept) is
			begin
				case (CS2) is
					when STATE_IDLE_2       => if (signalSTART = '1') then NS2 <= STATE_INIT;
													   else NS2 <= STATE_IDLE_2;
													   end if;
													
					when STATE_INIT	      => NS2 <= STATE_RCV_DATA;
					
					when STATE_RCV_DATA     => if (dFlag = '1') then NS2 <= STATE_RCV_P;
						   						   else NS2 <= STATE_RCV_DATA;
							   					   end if;
														
					when STATE_RCV_P        => if (PFlag = '1') then NS2 <= STATE_CHK_DX;
						   						   else NS2 <= STATE_RCV_P;
							   					   end if;
														
					when STATE_CHK_DX       =>	if (RXerror = '0')then NS2 <= STATE_TRANSMIT_DX;
													   else NS2 <= STATE_IDLE_2;
													   end if;
														
					when STATE_TRANSMIT_DX	=> if (accept = '1') then NS2 <= STATE_WAIT_END;
													   else NS2 <= STATE_TRANSMIT_DX;
														end if;		

					when STATE_WAIT_END	   => if (accept = '1') then NS2 <= STATE_WAIT_END;
													   else NS2 <= STATE_IDLE_2;
													   end if;	
				end case;
			end process;
		
		busy  <= '1' when ((CS2 = STATE_CHK_DX) or 
								 (CS2 = STATE_TRANSMIT_DX) or 
								 (CS2 = STATE_WAIT_END)) else '0';
		DXval <= '1' when (CS2 = STATE_TRANSMIT_DX) else '0';
		wr    <= '1' when ((CS2 = STATE_RCV_DATA) or 
								 (CS2 = STATE_CHK_DX) or 
								 (CS2 = STATE_RCV_P)) else '0';
		init  <= '1' when (CS2 = STATE_INIT) else '0';


end Behavioral;
