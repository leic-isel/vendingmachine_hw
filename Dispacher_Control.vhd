----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    15:15:33 01/27/2022 
-- Design Name: 
-- Module Name:    Dispacher_Control - Behavioral 
-- Project Name: 	 Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Dispatcher_Control is
    Port ( CLK  : in  STD_LOGIC;
			  RST  : in  STD_LOGIC;
			  Fsh  : in  STD_LOGIC;
           Dval : in  STD_LOGIC;
           Din  : in  STD_LOGIC_VECTOR (5 downto 0);
           WrD  : out STD_LOGIC;
           WrL  : out STD_LOGIC;
           Dout : out STD_LOGIC_VECTOR (4 downto 0);
           done : out STD_LOGIC);
end Dispatcher_Control;

architecture Behavioral of Dispatcher_Control is

type STATE_TYPE is (STATE_IDLE, STATE_RUN, STATE_LCD, STATE_DISPENSER,
						  STATE_EJ, STATE_EJ_FN, STATE_POST_EJ, STATE_POST_FN,
						  STATE_DONE);

signal CS, NS : STATE_TYPE;

begin

	State_Transitions : process (CLK, RST, CS)
		begin 
			if (RST = '1') then CS <= STATE_IDLE;
			elsif rising_edge(CLK) then CS <= NS;
			end if;
	   end process;

	Next_State_Evaluation : process (CS, Dval, Din)
		begin
			case (CS) is
				
				when STATE_IDLE		=> if (Dval = '1') then NS <= STATE_RUN;
												else NS <= STATE_IDLE;
												end if;
				
				when STATE_RUN       => if (Din(0) = '1') then NS <=  STATE_LCD;
											   elsif (Din(0) = '0') then NS <=  STATE_DISPENSER;
											   else NS <=  STATE_RUN;
											   end if;
										
				when STATE_LCD       => NS <= STATE_DONE;
				
				when STATE_DISPENSER => NS <= STATE_EJ;
												
				when STATE_Ej        => NS <= STATE_EJ_FN;
				
				when STATE_EJ_FN     => NS <= STATE_POST_EJ;
				
				when STATE_POST_EJ   => if (Fsh = '1') then NS <= STATE_POST_FN;
												else NS <=  STATE_POST_EJ;
												end if;
				
				when STATE_POST_FN   => if (Fsh = '1') then NS <= STATE_POST_FN;
												else NS <=  STATE_DONE;
												end if;
				
				when STATE_DONE	   => if (Dval = '1') then NS <= STATE_DONE;
												else NS <=  STATE_IDLE;
												end if;
				
			end case;
		end process;
	
		-- signal assignments 	
		WrL   <= Din(0) when CS = STATE_LCD else '0';
		WrD 	<= '1' when ((CS = STATE_DISPENSER) or
								 (CS = STATE_EJ) or
								 (CS = STATE_EJ_FN) or
								 (CS = STATE_POST_EJ)) else '0';
		done 	<= '1' when  (CS = STATE_DONE) else '0';
		Dout 	<= Din(5 downto 1);

end Behavioral;
