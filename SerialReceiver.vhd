----------------------------------------------------------------------------------
-- Company: 	    ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    22:24:59 12/13/2021 
-- Design Name: 
-- Module Name:    SerialReceiver - Behavioral 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SerialReceiver is
    Port ( CLK    : in  STD_LOGIC;
			  RST    : in  STD_LOGIC;
			  SDX    : in  STD_LOGIC;
           SCLK   : in  STD_LOGIC;
           accept : in  STD_LOGIC;
           Data   : out STD_LOGIC_VECTOR (5 downto 0);
           DXval  : out STD_LOGIC;
           busy   : out STD_LOGIC);
end SerialReceiver;

architecture Structural of SerialReceiver is

	component SerialControl is
		 Port ( CLK     : in  STD_LOGIC;
				  RST     : in  STD_LOGIC;
				  SDX     : in  STD_LOGIC;
				  SCLK    : in  STD_LOGIC; 
				  accept  : in  STD_LOGIC;
				  pFlag   : in  STD_LOGIC;
				  dFlag   : in  STD_LOGIC;
				  RXerror : in  STD_LOGIC;
				  wr      : out STD_LOGIC; 
				  init    : out STD_LOGIC; 
				  DXval   : out STD_LOGIC; 
				  busy    : out STD_LOGIC); 
	end component;

	component SerialCounter is
		 Port ( CLK     : in  STD_LOGIC;
				  CLR     : in  STD_LOGIC;
				  Qresult : out STD_LOGIC_VECTOR (2 downto 0));
	end component;
	
	component Compare5 is
		 Port ( input  : in  STD_LOGIC_VECTOR (2 downto 0);
				  output : out STD_LOGIC);
	end component;
	
	component Compare6 is
		 Port ( input  : in  STD_LOGIC_VECTOR (2 downto 0);
				  output : out STD_LOGIC);
	end component;
	
	component Compare7 is
		 Port ( input  : in  STD_LOGIC_VECTOR (2 downto 0);
				  output : out STD_LOGIC);
	end component;
	
	component MUX2_1 is
		Generic (WIDTH : POSITIVE :=1);
		 Port ( I0  : in  STD_LOGIC_VECTOR (Width -1 downto 0);
				  I1  : in  STD_LOGIC_VECTOR (Width -1 downto 0);
				  sel : in  STD_LOGIC;
				  Y   : out STD_LOGIC_VECTOR(Width -1 downto 0));
	end component;
	
	component ParityCheck is
		 Port ( SCLK : in  STD_LOGIC;
				  data : in  STD_LOGIC;
				  init : in  STD_LOGIC;
				  err  : out STD_LOGIC);
	end component;
	
	component DataStorage is
		 Port ( CLK    : in  STD_LOGIC;
				  data   : in  STD_LOGIC;
				  enable : in  STD_LOGIC;
				  Ain    : in  STD_LOGIC_VECTOR (2 downto 0);
				  Dout   : out STD_LOGIC_VECTOR (5 downto 0));
	end component;

	signal wr_enable       : STD_LOGIC;
	signal signal_init     : STD_LOGIC;
	signal err_RXerror     : STD_LOGIC;
	signal signal_counter  : STD_LOGIC_VECTOR (2 downto 0);
	signal signal_pFlag    : STD_LOGIC_VECTOR (0 downto 0);
	signal signal_dFlag    : STD_LOGIC_VECTOR (0 downto 0);
	signal signal5         : STD_LOGIC_VECTOR (0 downto 0);
	signal signal6         : STD_LOGIC_VECTOR (0 downto 0);
	signal signal7         : STD_LOGIC_VECTOR (0 downto 0);
	signal signal_data     : STD_LOGIC_VECTOR (5 downto 0);
	signal signal_operandA : STD_LOGIC_VECTOR (2 downto 0);

begin

	U0_SerialControl: SerialControl
		Port map (CLK => CLK, RST => RST, SDX => SDX, SCLK => SCLK, accept => accept,
				    pFlag => signal_pFlag(0), dFlag => signal_dFlag(0), wr => wr_enable,
					 init => signal_init, DXval => DXval, busy => busy, RXerror => err_RXerror);

	U1_SerialCounter: SerialCounter
		Port map (CLK => SCLK, CLR => signal_init, Qresult => signal_counter);

	U2_Compare5: Compare5
		Port map (input => signal_counter, output => signal5(0));

	U3_Compare6: Compare6
		Port map (input => signal_counter, output => signal6(0));

	U4_Compare7: Compare7
		Port map (input => signal_counter, output => signal7(0));
		
	U5_MUX_signal5and6: MUX2_1
		Generic map (WIDTH => 1)
		Port map (I0 => signal5, I1 => signal6, sel	 => signal_data(0), Y => signal_dFlag);

	U6_MUX_signal6and7: MUX2_1
		Generic map (WIDTH => 1)
		Port map (I0 => signal6, I1 => signal7, sel => signal_data(0), Y => signal_pFlag);
					 
	U7_ParityCheck: ParityCheck
		Port map (SCLK => SCLK, data => SDX, init => signal_init, err => err_RXerror);

	U8_DataStorage: DataStorage
		Port map (data => SDX, CLK => SCLK, enable => wr_enable, Ain => signal_counter,
			       Dout => signal_data);

	Data <= signal_data;

end Structural;
