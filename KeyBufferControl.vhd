----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    16:39:19 11/09/2021 
-- Design Name: 
-- Module Name:    KeyBufferControl - Behavioral 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KeyBufferControl is
    Port ( CLK  : in  STD_LOGIC;
           RST  : in  STD_LOGIC;
           DAV  : in  STD_LOGIC;
           ACK  : in  STD_LOGIC;
           Wreg : out STD_LOGIC;
           DAC  : out STD_LOGIC;
           Dval : out STD_LOGIC);
end KeyBufferControl;

architecture Behavioral of KeyBufferControl is

	type STATE_TYPE is (STATE_IDLE, STATE_WR, STATE_DVAL_DAC, STATE_DVAL, STATE_ACK);

	signal CS, NS : STATE_TYPE;

begin
	State_Transition : process (CLK, RST)
	begin
		if (RST = '1') then CS <= STATE_IDLE;
		elsif rising_edge(CLK) then CS <= NS;
		end if;
	end process;

	Next_State_Evaluation : process (CS, DAV, ACK)
	begin
		case (CS) is
			when STATE_IDLE     => if (DAV = '1') then NS <= STATE_WR;
									     else NS <= STATE_IDLE;
									     end if;
									 
			when STATE_WR       => NS <= STATE_DVAL_DAC;
			
			when STATE_DVAL_DAC => if (DAV = '1') then NS <= STATE_DVAL_DAC;
										  else NS <= STATE_DVAL;
										  end if;
										  
			when STATE_DVAL     => if (ACK = '1') then NS <= STATE_ACK;
									     else NS <= STATE_DVAL;
									     end if;
									 
			when STATE_ACK      => if (ACK = '1') then NS <= STATE_ACK;
									     else NS <= STATE_IDLE;
									     end if;
		end case;
	end process;
	
	-- signal assigments statements
	Dval <= '1' when (CS = STATE_DVAL_DAC or CS = STATE_DVAL) else '0';
	DAC  <= '1' when (CS = STATE_DVAL_DAC) else '0';
	Wreg <= '1' when (CS = STATE_WR) else '0';

end Behavioral;
