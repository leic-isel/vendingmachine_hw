----------------------------------------------------------------------------------
-- Company: 		 ISEL
-- Engineer: 		 Grupo 6
-- 
-- Create Date:    14:25:45 01/27/2022 
-- Design Name: 
-- Module Name:    Dispenser - Behavioral 
-- Project Name:   Vending Machine
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Dispenser is
    Port ( Ej     : in  STD_LOGIC;
           PId    : in  STD_LOGIC_VECTOR (3 downto 0);
			  Fn     : out STD_LOGIC;			  
			  FnIN   : in  STD_LOGIC;           
			  EjOUT  : out STD_LOGIC;
			  PIdOUT : out STD_LOGIC_VECTOR (3 downto 0));
end Dispenser;

architecture Behavioral of Dispenser is
	
begin

	Fn <= FnIN;
	EjOUT <= Ej;
	PIdOUT <= PId;

end Behavioral;
